#include <stdio.h>
#include <stdlib.h>
#include  <freertos/FreeRTOS.h>
#include  <freertos/task.h>
#include  <esp_log.h>
#include  <nvs_flash.h>

#include  <esp_rmaker_core.h>
#include  <esp_rmaker_standard_params.h>
#include  <esp_rmaker_standard_devices.h>
#include  <esp_rmaker_schedule.h>
#include "driver/gpio.h"
#include  <app_wifi.h>

#include  "app_priv.h"

esp_rmaker_device_t *TestLightDevice;
esp_rmaker_param_t *response_param;
esp_rmaker_param_t *request_param;
static  const  char *TAG = " app_main " ;

esp_rmaker_device_t *TestLightDecice;
#define TEST_DELAY 5000
char Cpts1 = 0 ,Count1 = 0, Cpts2 = 0 ,Count2 = 0;
char lastSecondinput ,lastFirstInput ;
char firstInputLevel,newInputLevel,secondInputLevel ;
bool  firstState = true,secondState = true ;
void testLight(char input)
{   
    
    
        switch(input) {
            case 1 :
                firstState = !firstState;
                printf("firstInputLevel %d      " , firstInputLevel);
                 /* Set the GPIO as a push/pull output */
                gpio_set_level(FIRST_OUTPUT, firstState);
                vTaskDelay(60000 / portTICK_PERIOD_MS);
                newInputLevel = gpio_get_level(FIRST_INPUT);
                vTaskDelay(2000 / portTICK_PERIOD_MS);
                printf("newInputLevel %d\n" , newInputLevel);
                
                if (newInputLevel == firstInputLevel) {
                    Count1++;
                   
                }
                firstInputLevel = newInputLevel;
                Cpts1++;
                printf(" NB° Iteration First Out : %d ,  NB° Error  First Out: %d \n ",Cpts1,Count1);
            break;
            case 2 :
                secondState = !secondState;
                printf("secondInputLevel %d      " , secondInputLevel);
                 /* Set the GPIO as a push/pull output */
                gpio_set_level(SECONDE_OUTPUT, secondState);
                vTaskDelay(60000 / portTICK_PERIOD_MS);
                newInputLevel = gpio_get_level(SECONDE_INPUT);
                vTaskDelay(2000 / portTICK_PERIOD_MS);
                printf("newInputLevel %d\n" , newInputLevel);
                
                if (newInputLevel == secondInputLevel) {
                    Count2++;
                   
                }
                secondInputLevel = newInputLevel;
                Cpts2++;
                printf(" NB° Iteration  Second Out: %d ,  NB° Error Second Out: %d \n ",Cpts1,Count2);
        }
    
}

void testLatchLight(char input)
{   
    FILE* fichier = NULL;
    //fichier = fopen("C:\test\test.txt", "a");
    char inputLevel = 0;
    while(1) {
        switch(input) {
            case 1 :
                gpio_pad_select_gpio(FIRST_OUTPUT);
                /* Set the GPIO as a push/pull output */
                gpio_set_direction(FIRST_OUTPUT, GPIO_MODE_OUTPUT);
                gpio_set_level(FIRST_OUTPUT, 0);
                printf( "\n-FIRST_Output == 0\n");           
                vTaskDelay(TEST_DELAY / portTICK_PERIOD_MS);
                inputLevel = gpio_get_level(FIRST_INPUT);
                gpio_set_level(FIRST_OUTPUT, 1);
                vTaskDelay(TEST_DELAY / portTICK_PERIOD_MS);
                printf("-FIRST_INPUT == %d",inputLevel);
                if(inputLevel == lastFirstInput ) {
                    printf ("error First Output");
                }
                lastFirstInput = inputLevel;       
            break;
            case 2 :
                gpio_pad_select_gpio(SECONDE_OUTPUT);
                /* Set the GPIO as a push/pull output */
                gpio_set_direction(SECONDE_OUTPUT, GPIO_MODE_OUTPUT);
                gpio_set_level(FIRST_OUTPUT, 0);
                printf( "\n-SECOND_OUTPUT == 0\n");           
                vTaskDelay(1000 / portTICK_PERIOD_MS);
                inputLevel = gpio_get_level(SECONDE_OUTPUT);
                gpio_set_level(FIRST_OUTPUT, 1);
                vTaskDelay(1000 / portTICK_PERIOD_MS);
                printf("-SECOND_OUTPUT == %d",inputLevel);
                if(inputLevel == lastSecondinput ) {
                    printf ("error Second Output");
                }
                lastSecondinput = inputLevel;
            break;
        }
    }
}

void app_main(void)
{   gpio_pad_select_gpio(FIRST_OUTPUT);
    gpio_set_direction(FIRST_OUTPUT, GPIO_MODE_OUTPUT);
    gpio_set_direction(FIRST_INPUT, GPIO_MODE_INPUT);
    gpio_set_level(FIRST_OUTPUT,firstState);

    gpio_pad_select_gpio(SECONDE_OUTPUT);
    gpio_set_direction(SECONDE_OUTPUT, GPIO_MODE_OUTPUT);
    gpio_set_direction(SECONDE_INPUT, GPIO_MODE_INPUT);
    gpio_set_level(SECONDE_OUTPUT,secondState);

    firstInputLevel = gpio_get_level(SECONDE_INPUT);
    secondInputLevel = gpio_get_level(SECONDE_INPUT);
  while (1) {
    testLight(2) ; 
    vTaskDelay(TEST_DELAY / portTICK_PERIOD_MS);
    //testLight(2) ; 
  }


}
