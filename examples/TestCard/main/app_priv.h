/*
   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#pragma once
#include <stdint.h>
#include <stdbool.h>
#include <esp_err.h>

#define FIRST_INPUT  17
#define SECONDE_INPUT  16

#define FIRST_OUTPUT 5
#define SECONDE_OUTPUT 4
#define INTR_OUTPUT 13
#define RESET_BUTTON 0
#define START_INPUT 13

void app_driver_init(void);
void startMeshDevice();
void startLigthDevice(int input);
void startSwitchDevice(int input);
void startLockDevice(int input);
void startLatchSwitchDevice(int input);
void testMesh(void);
void testLight(char input );
void lamp_set_gpio(int input);

esp_err_t app_driver_set_gpio(const char *name, bool state , bool *upStatus , bool *downStatus);