/* Simple GPIO Demo
   
   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

#include <sdkconfig.h>
#include <string.h>
#include <esp_log.h>

#include <app_reset.h>
#include "app_priv.h"

#define RMT_TX_CHANNEL RMT_CHANNEL_0
/* This is the button that is used for toggling the power */
#define BUTTON_GPIO          0
#define BUTTON_ACTIVE_LEVEL  0
/* This is the GPIO on which the power will be set */



#define WIFI_RESET_BUTTON_TIMEOUT       3
#define FACTORY_RESET_BUTTON_TIMEOUT    10

esp_err_t app_driver_set_gpio(const char *name, bool state , bool *upStatus , bool *downStatus)
{
    if (strcmp(name, "up") == 0) {
        *upStatus = state;
        if(*downStatus == true && state == true ) {
            *downStatus = false;
           gpio_set_level(FIRST_OUTPUT, *upStatus); 
           gpio_set_level(SECONDE_OUTPUT, *downStatus); 
        } else {
            gpio_set_level(FIRST_OUTPUT, *upStatus );
        }
    } else if (strcmp(name, "down") == 0) {
        *downStatus = state; 
        if(*upStatus == true && state == true) {
            *upStatus = false;
            gpio_set_level(FIRST_OUTPUT, *upStatus); 
            gpio_set_level(SECONDE_OUTPUT, *downStatus);
        }
        else {
            gpio_set_level(SECONDE_OUTPUT, state);
        }
        
    }else if (strcmp(name, "stop") == 0)  {
            if(state == true) {
            *downStatus = false;
            *upStatus = false;  
            gpio_set_level(FIRST_OUTPUT,*upStatus); 
            gpio_set_level(SECONDE_OUTPUT, *downStatus);
            }
    }
     else {
        return ESP_FAIL;
    }
    return ESP_OK;
}

void app_driver_init()
{
   

    /* Configure power */
    gpio_config_t io_conf = {
        .mode = GPIO_MODE_OUTPUT,
        .pull_up_en = 1,
    };
    uint64_t pin_mask = (((uint64_t)1 <<FIRST_OUTPUT ) | ((uint64_t)1 << SECONDE_OUTPUT) );
    io_conf.pin_bit_mask = pin_mask;
    /* Configure the GPIO */
    gpio_config(&io_conf);
    gpio_set_level(FIRST_OUTPUT, false);
    gpio_set_level(SECONDE_OUTPUT, false);
    
}
