/* 

   
*/

#include <string.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <esp_log.h>
#include <nvs_flash.h>
#include <esp_rmaker_schedule.h>
#include "driver/gpio.h"
#include <esp_rmaker_storage.h>
#include <esp_rmaker_core.h>
#include <esp_rmaker_standard_types.h>
#include <esp_rmaker_standard_devices.h>
#include <esp_rmaker_standard_params.h>
#include <app_wifi.h>
#include <iot_button.h>
#include <app_reset.h>
#include "app_priv.h"
#include <esp_timer.h>
#include <freertos/event_groups.h>
#include "freertos/timers.h"

#define BUTTON_ACTIVE_LEVEL 0

#define ESP_INTR_FLAG_DEFAULT 0
#define TEMPS_ANTI_REBOND_MS 250000
static const char *TAG = "app_main";
bool firstLedStatusReport = false;
bool secondLedStatusReport = false;
EventGroupHandle_t demo_eventgroup;
const int TX1_BIT = BIT0;
const int TX2_BIT = BIT1;
TimerHandle_t tmr;
int id = 1;
int interval = 100;

esp_rmaker_param_t *up_param;
esp_rmaker_param_t *down_param;
esp_rmaker_param_t *stop_param;
esp_rmaker_param_t *device_type_param;
esp_rmaker_device_t *firstDevice;
esp_rmaker_device_t *seconde_device;
esp_rmaker_device_t *config_device;
esp_rmaker_node_t *node;
char *deviceType;

bool firstOutputStatus = false;
bool secondeOutputStatus = false;
TaskHandle_t ISR = NULL;
TaskHandle_t ISR1 = NULL;
button_handle_t firstInput;
button_handle_t secondeInput;
int firstinputLevel;
int secondInputLevel;
int firstDeviceinput = 1;
int secondeDeviceinput = 2;
unsigned long firstDateAntiRebond = 0;
unsigned long secondDateAntiRebond = 0;
bool updatefromcloud = false;
TaskHandle_t ISRA = NULL;
TaskHandle_t ISRB = NULL;
// interrupt service routine, called when the button is pressed
void IRAM_ATTR button_A_isr_handler(void *arg)
{

    xTaskResumeFromISR(ISRA); //to resume a suspended task that can be called from within an ISR
}
void IRAM_ATTR button_B_isr_handler(void *arg)
{

    xTaskResumeFromISR(ISRB); //to resume a suspended task that can be called from within an ISR
}

void IRAM_ATTR up_isr_handler(void *arg)
{
    xTaskResumeFromISR(ISR);
}

void IRAM_ATTR down_isr_handler(void *arg)
{
    xTaskResumeFromISR(ISR1);
}

/* Callback to handle commands received from the RainMaker cloud */
static esp_err_t write_cb(const esp_rmaker_device_t *device, const esp_rmaker_param_t *param,
                          const esp_rmaker_param_val_t val, void *priv_data, esp_rmaker_write_ctx_t *ctx)
{
    deviceType = esp_rmaker_storage_get("device_type");

    const char *device_name = esp_rmaker_device_get_name(device);
    char *param_name = esp_rmaker_param_get_name(param);
    if (strcmp(param_name, "device_type") == 0)
    {
        if (strcmp(deviceType, "config") == 0)
        {
            ESP_LOGI(TAG, "device not updated  %s", deviceType);
            deviceType = val.val.s;
            esp_rmaker_storage_set("device_type", deviceType, strlen(deviceType));
            if (strcmp(val.val.s, "motor") == 0)
            {
                startMeshDevice();
                esp_rmaker_node_add_device(node, firstDevice);
            }
            if (strcmp(val.val.s, "lamp") == 0)
            {
                startLigthDevice(1);
                esp_rmaker_node_add_device(node, firstDevice);
            }
            if (strcmp(val.val.s, "two-lamp") == 0)
            {
                startLigthDevice(1);
                startLigthDevice(2);
                esp_rmaker_node_add_device(node, firstDevice);
                esp_rmaker_node_add_device(node, seconde_device);
            }
            if (strcmp(val.val.s, "switch") == 0)
            {
                startSwitchDevice(1);
                esp_rmaker_node_add_device(node, firstDevice);
            }
            if (strcmp(val.val.s, "lamp-switch") == 0)
            {
                startLigthDevice(1);
                startSwitchDevice(2);
                esp_rmaker_node_add_device(node, firstDevice);
                esp_rmaker_node_add_device(node, seconde_device);
            }
            if (strcmp(val.val.s, "two-switch") == 0)
            {
                startSwitchDevice(1);
                startSwitchDevice(2);
                esp_rmaker_node_add_device(node, firstDevice);
                esp_rmaker_node_add_device(node, seconde_device);
            }
            if (strcmp(val.val.s, "lock") == 0)
            {
                startLockDevice(1);
                esp_rmaker_node_add_device(node, firstDevice);
            }
            if (strcmp(val.val.s, "two-lock") == 0)
            {
                startLockDevice(1);
                startLockDevice(2);
                esp_rmaker_node_add_device(node, firstDevice);
                esp_rmaker_node_add_device(node, seconde_device);
            }
            if (strcmp(val.val.s, "lamp-lock") == 0)
            {
                startLigthDevice(1);
                startLockDevice(2);
                esp_rmaker_node_add_device(node, firstDevice);
                esp_rmaker_node_add_device(node, seconde_device);
            }
            if (strcmp(val.val.s, "switch-lock") == 0)
            {
                startSwitchDevice(1);
                startLockDevice(2);
                esp_rmaker_node_add_device(node, firstDevice);
                esp_rmaker_node_add_device(node, seconde_device);
            }
            if (strcmp(val.val.s, "LatchSwitch") == 0)
            {
                startLatchSwitchDevice(1);
                esp_rmaker_node_add_device(node, firstDevice);
            }
            if (strcmp(val.val.s, "LatchSwitch_Lock") == 0)
            {
                startLatchSwitchDevice(1);
                startLockDevice(2);
                esp_rmaker_node_add_device(node, firstDevice);
                esp_rmaker_node_add_device(node, seconde_device);
            }
            /* Added by afef*/
            if (strcmp(val.val.s, "Two_LatchSwitch") == 0)
            {
                startLatchSwitchDevice(1);
                startLatchSwitchDevice(2);
                esp_rmaker_node_add_device(node, firstDevice);
                esp_rmaker_node_add_device(node, seconde_device);
            }

            /*Remove a device from a node*/
            esp_rmaker_node_remove_device(node, config_device);
            /*Report the node details to the cloud*/
            esp_rmaker_report_node_details();
        }
    }
    if (ctx)
    {
        ESP_LOGI(TAG, "Received write request via : %s", esp_rmaker_device_cb_src_to_str(ctx->src));
    }
    if (strcmp(device_name, "firstLight") == 0)
    {
        firstOutputStatus = val.val.b;
        updatefromcloud = true;
        gpio_set_level(FIRST_OUTPUT, firstOutputStatus);
        /* Update and report a parameter
        Calling this API will update the parameter and report it to ESP RainMaker cloud. 
        This should be used whenever there is any local change. */
        esp_rmaker_param_update_and_report(param, esp_rmaker_bool(firstOutputStatus));
    }
    if (strcmp(device_name, "secondeLight") == 0)
    {
        secondeOutputStatus = val.val.b;
        updatefromcloud = true;

        gpio_set_level(SECONDE_OUTPUT, secondeOutputStatus);
        esp_rmaker_param_update_and_report(param, esp_rmaker_bool(secondeOutputStatus));
    }
    if (strcmp(device_name, "firstSwitch") == 0)
    {
        firstOutputStatus = val.val.b;
        updatefromcloud = true;
        gpio_set_level(FIRST_OUTPUT, firstOutputStatus);
        esp_rmaker_param_update_and_report(param, esp_rmaker_bool(firstOutputStatus));
    }
    if (strcmp(device_name, "secondeSwitch") == 0)
    {
        secondeOutputStatus = val.val.b;
        updatefromcloud = true;

        gpio_set_level(SECONDE_OUTPUT, secondeOutputStatus);
        esp_rmaker_param_update_and_report(param, esp_rmaker_bool(secondeOutputStatus));
    }
    if (strcmp(device_name, "firstLock") == 0)
    {
        updatefromcloud = true;
        gpio_set_level(FIRST_OUTPUT, true);
        //2*T = 2*200Ms
        vTaskDelay(1000 / portTICK_PERIOD_MS);
        gpio_set_level(FIRST_OUTPUT, false);
        esp_rmaker_param_update_and_report(param, esp_rmaker_bool(firstOutputStatus));
    }
    if (strcmp(device_name, "secondeLock") == 0)
    {
        updatefromcloud = true;
       esp_err_t  ok =  gpio_set_level(SECONDE_OUTPUT, true);

        vTaskDelay(400 / portTICK_PERIOD_MS);
        esp_err_t  ok2=  gpio_set_level(SECONDE_OUTPUT, false);
        
    }
    if (strcmp(device_name, "motor") == 0)
    {
        if (app_driver_set_gpio(esp_rmaker_param_get_name(param), val.val.b, &firstOutputStatus, &secondeOutputStatus) == ESP_OK)
        {
            esp_rmaker_param_update_and_report(up_param, esp_rmaker_bool(firstOutputStatus));
            esp_rmaker_param_update_and_report(down_param, esp_rmaker_bool(secondeOutputStatus));
        }
    }
    if (strcmp(device_name, "FirstLatchSwitch") == 0)
    {
        firstOutputStatus = val.val.b;
        updatefromcloud = true;
        gpio_set_level(FIRST_OUTPUT, firstOutputStatus);
        /* Update and report a parameter*/
        esp_rmaker_param_update_and_report(param, esp_rmaker_bool(firstOutputStatus));
    }
    if (strcmp(device_name, "SecondLatchSwitch") == 0)
    {
        secondeOutputStatus = val.val.b;
        updatefromcloud = true;
        gpio_set_level(SECONDE_OUTPUT, secondeOutputStatus);
        esp_rmaker_param_update_and_report(param, esp_rmaker_bool(secondeOutputStatus));
    }
    if (!deviceType)
    {
        printf("test  no devicd configured");
    }
    else
    {
        printf("test  %s\n", deviceType);
    }
    return ESP_OK;
}

void vTimerCallback(void *arg)
{
    if (firstLedStatusReport || secondLedStatusReport)
    {
        if (strcmp(deviceType, "motor") == 0)
        {
            esp_rmaker_param_update_and_report(
                up_param,
                esp_rmaker_bool(firstOutputStatus));
            esp_rmaker_param_update_and_report(
                down_param,
                esp_rmaker_bool(secondeOutputStatus));
            firstLedStatusReport = false;
        }
        if (strcmp(deviceType, "lamp") == 0)
        {

            esp_rmaker_param_update_and_report(
                esp_rmaker_device_get_param_by_name(firstDevice, "Power"),
                esp_rmaker_bool(firstOutputStatus));
            firstLedStatusReport = false;
        }
        if (strcmp(deviceType, "two-lamp") == 0)
        {
            esp_rmaker_param_update_and_report(
                esp_rmaker_device_get_param_by_name(firstDevice, "Power"),
                esp_rmaker_bool(firstOutputStatus));
            esp_rmaker_param_update_and_report(
                esp_rmaker_device_get_param_by_name(seconde_device, "Power"),
                esp_rmaker_bool(secondeOutputStatus));
            firstLedStatusReport = false;
            secondLedStatusReport = false;
        }
        
    }
}

void rx_sync_task(void *arg)
{
    EventBits_t bits;

    while (1)
    {
        // read Bits and clear
        bits = xEventGroupWaitBits(demo_eventgroup, TX1_BIT | TX2_BIT, pdTRUE, pdTRUE, 1000 / portTICK_RATE_MS); // max wait 60s
        if (firstLedStatusReport || secondLedStatusReport)
        {
            if (bits == TX1_BIT)
            { // xWaitForAllBits == pdTRUE, so we wait for TX1_BIT and TX2_BIT so all other is timeout

                if (strcmp(deviceType, "motor") == 0)
                {
                    esp_rmaker_param_update_and_report(
                        up_param,
                        esp_rmaker_bool(firstOutputStatus));
                    esp_rmaker_param_update_and_report(
                        down_param,
                        esp_rmaker_bool(secondeOutputStatus));
                        firstLedStatusReport = false;
                }
                else
                {
                    esp_rmaker_param_update_and_report(
                        esp_rmaker_device_get_param_by_name(firstDevice, "Power"),
                        esp_rmaker_bool(firstOutputStatus));
                    firstLedStatusReport = false;
                    esp_rmaker_param_update_and_report(
                        esp_rmaker_device_get_param_by_name(seconde_device, "Power"),
                        esp_rmaker_bool(secondeOutputStatus));

                    secondLedStatusReport = false;
                }
            }
        }
    }
}

void push_up_btn_cb(void *arg)
{
    esp_err_t errUp;
    esp_err_t errDown;

    if (secondeOutputStatus == false)
    {
        firstOutputStatus = !firstOutputStatus;
        errUp = gpio_set_level(FIRST_OUTPUT, firstOutputStatus);
        ESP_LOGE("test", "physical output up updated");
        if (errUp == ESP_OK)
        {
            firstLedStatusReport = true;
            xEventGroupSetBits(demo_eventgroup, TX1_BIT);
        }
    }
    else
    {
        firstOutputStatus = !firstOutputStatus;
        secondeOutputStatus = false;
        errUp = gpio_set_level(FIRST_OUTPUT, firstOutputStatus);
        errDown = gpio_set_level(SECONDE_OUTPUT, secondeOutputStatus);
        ESP_LOGE("test", "both physical output  updated");
        if (errUp == ESP_OK && errDown == ESP_OK)
        {
            firstLedStatusReport = true;
            xEventGroupSetBits(demo_eventgroup, TX1_BIT);
        }
    }
}
void push_down_btn_cb(void *arg)
{
    esp_err_t errUp;
    esp_err_t errDown;

    if (firstOutputStatus == false)
    {
        secondeOutputStatus = !secondeOutputStatus;
        errUp = gpio_set_level(SECONDE_OUTPUT, secondeOutputStatus);
        ESP_LOGE("test", "physical output down updated");
        if (errUp == ESP_OK)
        {
            firstLedStatusReport = true;
            xEventGroupSetBits(demo_eventgroup, TX1_BIT);
        }
    }
    else
    {
        secondeOutputStatus = !secondeOutputStatus;
        firstOutputStatus = false;
        errUp = gpio_set_level(FIRST_OUTPUT, firstOutputStatus);
        errDown = gpio_set_level(SECONDE_OUTPUT, secondeOutputStatus);
        ESP_LOGE("test", "both output down updated");
        if (errUp == ESP_OK && errDown == ESP_OK)
        {
            firstLedStatusReport = true;
            xEventGroupSetBits(demo_eventgroup, TX1_BIT);
        }
    }
}

void changeFirstLigthOutputStatus(void *arg)
{
    esp_err_t errUp;
    int newinputLevel = gpio_get_level(FIRST_INPUT);
    if (firstinputLevel == newinputLevel)
    {
        return;
    }
    else
    {
        firstinputLevel = newinputLevel;
        firstOutputStatus = !firstOutputStatus;
        errUp = gpio_set_level(FIRST_OUTPUT, firstOutputStatus);
        if (errUp == ESP_OK)
        {
            firstLedStatusReport = true;
            xEventGroupSetBits(demo_eventgroup, TX1_BIT);
        }
    }
}

void changeSecondLigthOutputStatus(void *arg)
{
    esp_err_t errUp;
    if (secondInputLevel == gpio_get_level(SECONDE_INPUT))
    {
        return;
    }
    else
    {
        secondInputLevel = gpio_get_level(SECONDE_INPUT);
        secondeOutputStatus = !secondeOutputStatus;
        errUp = gpio_set_level(SECONDE_OUTPUT, secondeOutputStatus);
        if (errUp == ESP_OK)
        {
            secondLedStatusReport = true;
            xEventGroupSetBits(demo_eventgroup, TX2_BIT);
        }
    }
}

void startMeshDevice()
{

    if (firstInput)
    {
        iot_button_set_evt_cb(firstInput, BUTTON_CB_TAP, push_up_btn_cb, NULL);
    }

    if (secondeInput)
    {
        iot_button_set_evt_cb(secondeInput, BUTTON_CB_TAP, push_down_btn_cb, NULL);
    }

    firstDevice = esp_rmaker_device_create("motor", ESP_RMAKER_DEVICE_MESH, NULL);
    /*Ajouter des rappels pour un appareil / service*/
    esp_rmaker_device_add_cb(firstDevice, write_cb, NULL);

    up_param = esp_rmaker_param_create("up", NULL, esp_rmaker_bool(false), PROP_FLAG_READ | PROP_FLAG_WRITE);
    esp_rmaker_param_add_ui_type(up_param, ESP_RMAKER_UI_TOGGLE);
    esp_rmaker_device_add_param(firstDevice, up_param);
    stop_param = esp_rmaker_param_create("stop", NULL, esp_rmaker_bool(false), PROP_FLAG_READ | PROP_FLAG_WRITE);
    esp_rmaker_param_add_ui_type(stop_param, ESP_RMAKER_UI_TOGGLE);
    esp_rmaker_device_add_param(firstDevice, stop_param);
    down_param = esp_rmaker_param_create("down", NULL, esp_rmaker_bool(false), PROP_FLAG_READ | PROP_FLAG_WRITE);
    esp_rmaker_param_add_ui_type(down_param, ESP_RMAKER_UI_TOGGLE);
    esp_rmaker_device_add_param(firstDevice, down_param);
}
void button_A_task(void *pvParameter)
{

    while (1)
    {
        vTaskSuspend(NULL);
        int newLevelInput = gpio_get_level(FIRST_INPUT);

        if (esp_timer_get_time() - firstDateAntiRebond > TEMPS_ANTI_REBOND_MS)
        {
            if (!(firstinputLevel == newLevelInput))
            {
                firstinputLevel = newLevelInput;
                firstOutputStatus = !firstOutputStatus;
                gpio_set_level(FIRST_OUTPUT, firstOutputStatus);
                firstDateAntiRebond = esp_timer_get_time();
                firstLedStatusReport = true;
                xEventGroupSetBits(demo_eventgroup, TX1_BIT);
            }
        }
    }
}

void button_B_task(void *pvParameter)
{

    while (1)
    {
        vTaskSuspend(NULL);
        int newLevelInput = gpio_get_level(SECONDE_INPUT);

        if (esp_timer_get_time() - secondDateAntiRebond > TEMPS_ANTI_REBOND_MS)
        {
            if (!(secondInputLevel == newLevelInput))
            {
                secondInputLevel = newLevelInput;
                secondeOutputStatus = !secondeOutputStatus;
                gpio_set_level(SECONDE_OUTPUT, secondeOutputStatus);
                secondDateAntiRebond = esp_timer_get_time();
                secondLedStatusReport = true;
                xEventGroupSetBits(demo_eventgroup, TX1_BIT);
            }
        }
    }
}

void button_FirstLatch_task(void *pvParameter)
{

    while (1)
    {
        vTaskSuspend(NULL);
        int newLevelInput = gpio_get_level(FIRST_INPUT);
        if (esp_timer_get_time() - firstDateAntiRebond > TEMPS_ANTI_REBOND_MS)
        {
            if (newLevelInput == 1)
            {
                firstinputLevel = newLevelInput;
                
            }
            else
            {
                if (!(firstinputLevel == newLevelInput))
                {
                    firstOutputStatus = !firstOutputStatus;
                    gpio_set_level(FIRST_OUTPUT, firstOutputStatus);
                    firstDateAntiRebond = esp_timer_get_time();
                    firstLedStatusReport = true;
                    xEventGroupSetBits(demo_eventgroup, TX1_BIT);
                    firstinputLevel = newLevelInput;
                }
            }
        }
    }
}

void button_SecondLatch_task(void *pvParameter)
{

    while (1)
    {
        vTaskSuspend(NULL);
        int newLevelInput = gpio_get_level(SECONDE_INPUT);

        if (esp_timer_get_time() - secondDateAntiRebond > TEMPS_ANTI_REBOND_MS)
        {
            if (newLevelInput == 1)
            {
                secondInputLevel = newLevelInput;
            }
            else
            {
                if (!(secondInputLevel == newLevelInput))
                {
                    secondeOutputStatus = !secondeOutputStatus;
                    gpio_set_level(SECONDE_OUTPUT, secondeOutputStatus);
                    secondDateAntiRebond = esp_timer_get_time();
                    secondLedStatusReport = true;
                    xEventGroupSetBits(demo_eventgroup, TX1_BIT);
                    secondInputLevel = newLevelInput;
                }
            }
        }
    }
}

void startLigthDevice(int input)
{
    switch (input)
    {
    case 1:
        if (firstInput)
        {
            firstinputLevel = gpio_get_level(FIRST_INPUT);
            gpio_pad_select_gpio(FIRST_INPUT);
            /* Set the GPIO as a push/pull input */
            gpio_set_direction(FIRST_INPUT, GPIO_MODE_INPUT);
            gpio_set_intr_type(FIRST_INPUT, GPIO_INTR_ANYEDGE);
            gpio_pullup_dis(FIRST_INPUT);
            gpio_pulldown_en(FIRST_INPUT);
            gpio_isr_handler_add(FIRST_INPUT, button_A_isr_handler, NULL);
            xTaskCreate(button_A_task, "button_A_task", 4096, NULL, 10, &ISRA);
        }
        /*Create a standard Lightbulb device*/
        firstDevice = esp_rmaker_lightbulb_device_create("firstLight", NULL, firstOutputStatus);
        esp_rmaker_device_add_cb(firstDevice, write_cb, NULL);
        break;
    case 2:
        if (secondeInput)
        {
            secondInputLevel = gpio_get_level(SECONDE_INPUT);
            gpio_pad_select_gpio(SECONDE_INPUT);
            /* Set the GPIO as a push/pull input */
            gpio_set_direction(SECONDE_INPUT, GPIO_MODE_INPUT);
            gpio_set_intr_type(SECONDE_INPUT, GPIO_INTR_ANYEDGE);
            gpio_pullup_dis(SECONDE_INPUT);
            gpio_pulldown_en(SECONDE_INPUT);
            gpio_isr_handler_add(SECONDE_INPUT, button_B_isr_handler, NULL);
            xTaskCreate(button_B_task, "button_B_task", 4096, NULL, 10, &ISRB);
        }
        /*Create a standard Lightbulb device*/
        seconde_device = esp_rmaker_lightbulb_device_create("secondeLight", NULL, true);
        esp_rmaker_device_add_cb(seconde_device, write_cb, NULL);
        break;
    }
}

void startSwitchDevice(int input)
{
    //Create standard Power param
    esp_rmaker_param_t *power_param = esp_rmaker_power_param_create(ESP_RMAKER_DEF_POWER_NAME, true);

    switch (input)
    {
    case 1:
        firstDevice = esp_rmaker_device_create("firstSwitch", ESP_RMAKER_DEVICE_SWITCH, NULL);
        esp_rmaker_device_add_cb(firstDevice, write_cb, NULL);
        esp_rmaker_device_add_param(firstDevice, esp_rmaker_name_param_create(ESP_RMAKER_DEF_NAME_PARAM, "Switch"));
        esp_rmaker_device_add_param(firstDevice, power_param);
        /* Assign the power parameter as the primary, so that it can be controlled from the
            * home screen of the phone apps.
        */
        esp_rmaker_device_assign_primary_param(firstDevice, power_param);
        break;
    case 2:
        seconde_device = esp_rmaker_device_create("secondeSwitch", ESP_RMAKER_DEVICE_SWITCH, NULL);
        esp_rmaker_device_add_cb(seconde_device, write_cb, NULL);
        esp_rmaker_device_add_param(seconde_device, esp_rmaker_name_param_create(ESP_RMAKER_DEF_NAME_PARAM, "Switch"));
        esp_rmaker_device_add_param(seconde_device, power_param);
        esp_rmaker_device_assign_primary_param(seconde_device, power_param);
        break;
    }
}

void startLockDevice(int input)
{

    esp_rmaker_param_t *power_param = esp_rmaker_param_create("up ", NULL, esp_rmaker_bool(false), PROP_FLAG_READ | PROP_FLAG_WRITE);
    esp_rmaker_param_add_ui_type(power_param, ESP_RMAKER_UI_KEY);
    switch (input)
    {
        case 1:
            firstDevice = esp_rmaker_device_create("firstLock", ESP_RMAKER_DEVICE_LOCK, NULL);
            esp_rmaker_device_add_cb(firstDevice, write_cb, NULL);
            esp_rmaker_device_add_param(firstDevice, esp_rmaker_name_param_create(ESP_RMAKER_DEF_NAME_PARAM, "Lock"));
            esp_rmaker_device_add_param(firstDevice, power_param);
            esp_rmaker_device_assign_primary_param(firstDevice, power_param);
        break;
        case 2:
            seconde_device = esp_rmaker_device_create("secondeLock", ESP_RMAKER_DEVICE_LOCK, NULL);
            esp_rmaker_device_add_cb(seconde_device, write_cb, NULL);
            esp_rmaker_device_add_param(seconde_device, esp_rmaker_name_param_create(ESP_RMAKER_DEF_NAME_PARAM, "Lock"));
            esp_rmaker_device_add_param(seconde_device, power_param);
            esp_rmaker_device_assign_primary_param(seconde_device, power_param);
            
        break;
    }
}
// added by afef
void startLatchSwitchDevice(int input)
{
    switch (input)
    {
    case 1:
        if (firstInput) {
            firstinputLevel = gpio_get_level(FIRST_INPUT);
            gpio_pad_select_gpio(FIRST_INPUT);
            /* Set the GPIO as a push/pull input */
            gpio_set_direction(FIRST_INPUT, GPIO_MODE_INPUT);
            gpio_set_intr_type(FIRST_INPUT, GPIO_INTR_ANYEDGE);
            gpio_pullup_dis(FIRST_INPUT);
            gpio_pulldown_dis(FIRST_INPUT);
            gpio_isr_handler_add(FIRST_INPUT, button_A_isr_handler, NULL);
            xTaskCreate(button_FirstLatch_task, "button_FirstLatch_task", 4096, NULL, 10, &ISRA);
        }
        /*Create a standard Lightbulb device*/
        firstDevice = esp_rmaker_lightbulb_device_create("FirstLatchSwitch", NULL, firstOutputStatus);
        esp_rmaker_device_add_cb(firstDevice, write_cb, NULL);
        break;
    case 2:
        if (secondeInput) {
            secondInputLevel = gpio_get_level(SECONDE_INPUT);
            gpio_pad_select_gpio(SECONDE_INPUT);
            /* Set the GPIO as a push/pull input */
            gpio_set_direction(SECONDE_INPUT, GPIO_MODE_INPUT);
            gpio_set_intr_type(SECONDE_INPUT, GPIO_INTR_ANYEDGE);
            gpio_pullup_dis(SECONDE_INPUT);
            gpio_pulldown_dis(SECONDE_INPUT);
            gpio_isr_handler_add(SECONDE_INPUT, button_B_isr_handler, NULL);
            xTaskCreate(button_SecondLatch_task, "button_SecondLatch_task", 4096, NULL, 10, &ISRB);
        }
        seconde_device = esp_rmaker_lightbulb_device_create("SecondLatchSwitch", NULL, secondeOutputStatus);
        esp_rmaker_device_add_cb(seconde_device, write_cb, NULL);
        break;
    }
}

void app_main()
{

    // install ISR service
    gpio_install_isr_service(ESP_INTR_FLAG_DEFAULT);
    firstInput = iot_button_create(FIRST_INPUT, BUTTON_ACTIVE_LEVEL);
    secondeInput = iot_button_create(SECONDE_INPUT, BUTTON_ACTIVE_LEVEL);
    button_handle_t btn_handle = iot_button_create(RESET_BUTTON, BUTTON_ACTIVE_LEVEL);
    app_reset_button_register(btn_handle, 3, 10);

    demo_eventgroup = xEventGroupCreate();
    tmr = xTimerCreate("MyTimer", interval, pdTRUE, (void *)id, vTimerCallback);
    xTaskCreate(rx_sync_task, "rx_sync_task", CONFIG_SYSTEM_EVENT_TASK_STACK_SIZE, NULL, 7, NULL);

    app_driver_init();

    /* Initialize NVS. */
    esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND)
    {
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }
    ESP_ERROR_CHECK(err);
    esp_rmaker_storage_init();
    if (esp_rmaker_storage_get("device_type"))
    {
        deviceType = esp_rmaker_storage_get("device_type");
    }

    /* Initialize Wi-Fi. Note that, this should be called before esp_rmaker_init()*/
    app_wifi_init();
    esp_timer_get_time();
    /* Initialize the ESP RainMaker Agent.
     * Note that this should be called after app_wifi_init() but before app_wifi_start()
     * */
    esp_rmaker_config_t rainmaker_cfg = {
        .enable_time_sync = false,
    };
    node = esp_rmaker_node_init(&rainmaker_cfg, "ESP RainMaker Device", "ESP32-S2-Saola-1");
    if (!node)
    {
        ESP_LOGE(TAG, "Could not initialise node. Aborting!!!");
        vTaskDelay(5000 / portTICK_PERIOD_MS);
        abort();
    }
    /* Create a Table of different device that can be configured.*/
    static const char *devices[] = {"config", "motor", "lamp", "two-lamp","LatchSwitch_Lock", "switch", "lamp-switch", "two-switch", "lock", "two-lock", "Two_LatchSwitch", "lamp-lock","LatchSwitch", "switch-lock"};
    /* Create a config device.*/
    config_device = esp_rmaker_device_create("config", NULL, NULL);

    /* Add the write callback for the device. We aren't registering any read callback yet as
     * it is for future use.*/
    esp_rmaker_device_add_cb(config_device, write_cb, NULL);
    device_type_param = esp_rmaker_param_create("device_type", NULL, esp_rmaker_str("motor"), PROP_FLAG_READ | PROP_FLAG_WRITE);
    esp_rmaker_param_add_ui_type(device_type_param, ESP_RMAKER_UI_DROPDOWN);
    esp_rmaker_param_add_valid_str_list(device_type_param, devices, (uint8_t)15);
    esp_rmaker_device_add_param(config_device, device_type_param);

    if (!esp_rmaker_storage_get("device_type") || strcmp(deviceType, "config") == 0)
    {
        ESP_LOGE("test", "no device configured");
        /* Add this config device to the node */
        esp_rmaker_node_add_device(node, config_device);
        esp_rmaker_storage_set("device_type", "config", strlen("config"));
        deviceType = esp_rmaker_storage_get("device_type");
        esp_rmaker_report_node_details();
    }
    /*on peut remplacer ce test de "if else" par un switch pour eviter a chaque fois de tester "deviceType"*/
    else if (strcmp(deviceType, "motor") == 0)
    {
        startMeshDevice();
        /* Add the motor device to the node */
        esp_rmaker_node_add_device(node, firstDevice);
    }
    else if (strcmp(deviceType, "lamp") == 0)
    {
        startLigthDevice(1);
        esp_rmaker_node_add_device(node, firstDevice);
    }
    else if (strcmp(deviceType, "two-lamp") == 0)
    {
        startLigthDevice(1);
        esp_rmaker_node_add_device(node, firstDevice);
        startLigthDevice(2);
        esp_rmaker_node_add_device(node, seconde_device);
    }
    else if (strcmp(deviceType, "switch") == 0)
    {
        startSwitchDevice(1);
        esp_rmaker_node_add_device(node, firstDevice);
    }
    else if (strcmp(deviceType, "two-switch") == 0)
    {
        startSwitchDevice(1);
        esp_rmaker_node_add_device(node, firstDevice);
        startSwitchDevice(2);
        esp_rmaker_node_add_device(node, seconde_device);
    }
    else if (strcmp(deviceType, "lamp-switch") == 0)
    {
        startLigthDevice(1);
        esp_rmaker_node_add_device(node, firstDevice);
        startSwitchDevice(2);
        esp_rmaker_node_add_device(node, seconde_device);
    }
    else if (strcmp(deviceType, "lock") == 0)
    {
        startLockDevice(1);
        esp_rmaker_node_add_device(node, firstDevice);
    }
    else if (strcmp(deviceType, "tow-lock") == 0)
    {
        startLockDevice(1);
        esp_rmaker_node_add_device(node, firstDevice);
        startLockDevice(2);
        esp_rmaker_node_add_device(node, seconde_device);
    }
    else if (strcmp(deviceType, "lamp-lock") == 0)
    {
        startLigthDevice(1);
        esp_rmaker_node_add_device(node, firstDevice);
        startLockDevice(2);
        esp_rmaker_node_add_device(node, seconde_device);
    }
    else if (strcmp(deviceType, "switch-lock") == 0)
    {
        startSwitchDevice(1);
        esp_rmaker_node_add_device(node, firstDevice);
        startLockDevice(2);
        esp_rmaker_node_add_device(node, seconde_device);
    }
    else if (strcmp(deviceType, "LatchSwitch") == 0)
    {
        startLatchSwitchDevice(1);
        /* Add this LatchSwitch device to the node */
        esp_rmaker_node_add_device(node, firstDevice);
    }
    else if (strcmp(deviceType, "Two_LatchSwitch") == 0)
    {
        startLatchSwitchDevice(1);
        esp_rmaker_node_add_device(node, firstDevice);
        startLatchSwitchDevice(2);
        esp_rmaker_node_add_device(node, seconde_device);
    }
    else if (strcmp(deviceType, "LatchSwitch_Lock") == 0)
    {
        startLatchSwitchDevice(1);
        esp_rmaker_node_add_device(node, firstDevice);
        startLockDevice(2);
        esp_rmaker_node_add_device(node, seconde_device);
    }
    /*Report the node details to the cloud*/
    esp_rmaker_report_node_details();
    /* Start the ESP RainMaker Agent */
    esp_rmaker_start();

    /* Enable scheduling.
     * Please note that you also need to set the timezone for schedules to work correctly.
     * Simplest option is to use the CONFIG_ESP_RMAKER_DEF_TIMEZONE config option.
     * Else, you can set the timezone using the API call `esp_rmaker_time_set_timezone("Asia/Shanghai");`
     * For more information on the various ways of setting timezone, please check
     */
    esp_rmaker_schedule_enable();

    /* Start the Wi-Fi.
     * If the node is provisioned, it will start connection attempts,
     * else, it will start Wi-Fi provisioning. The function will return
     * after a connection has been successfully established
     */
    err = app_wifi_start(POP_TYPE_RANDOM);
    if (err != ESP_OK)
    {
        ESP_LOGE(TAG, "Could not start Wifi. Aborting!!!");
        vTaskDelay(5000 / portTICK_PERIOD_MS);
        //app_wifi_start(POP_TYPE_RANDOM);
        abort();
    }
}
